# Symulacja robota klasy 3.0 w Gazebo.



Aby uruchomić symulację należy w osobnych terminalach uruchomić:

`roslaunch robot30 diff_wheeled_gazebo.launch`

oraz:

`rosrun robot30 key_teleop`

Sterowanie odbywa się przez klawiaturę, węzeł przyjmuje 6 klawiszy (potwierdzamy enterem). Ich wprowadzenie skutkuje odpowiednio zwiększeniem lub zmniejszeniem prękości koła:

- klawisze  `q`  i  `a`
- klawisze  `w`  i  `s`
- klawisze  `e`  i  `d`