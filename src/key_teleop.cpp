#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <iostream>

int main(int argc, char ** argv) {

  ros::init(argc, argv, "publish_velocity");
  ros::NodeHandle nh;

  ros::Publisher pub1 = nh.advertise<std_msgs::Float64>("/differential_wheeled_robot/front_right_wheel_joint_position_controller/command", 1000);

  ros::Publisher pub2 = nh.advertise<std_msgs::Float64>("/differential_wheeled_robot/front_left_wheel_joint_position_controller/command", 1000);

  ros::Publisher pub3 = nh.advertise<std_msgs::Float64>("/differential_wheeled_robot/back_left_wheel_joint_position_controller/command", 1000);

  std_msgs::Float64 msg1;
  std_msgs::Float64 msg2;
  std_msgs::Float64 msg3;

  
  while(ros::ok) {
  
    char key;
    std::cin >> key;

    switch (key){
    case 'q':
      msg1.data += 2.0;
      break;
    case 'w':
      msg2.data += 2.0;
      break;
    case 'e':
      msg3.data += 2.0;
      break;
    case 'a':
      msg1.data -= 2.0;
      break;
    case 's':
      msg2.data -= 2.0;
      break;
    case 'd':
      msg3.data -= 2.0;
      break;
    default:
      break;
    }
    
    pub1.publish(msg1);
    pub2.publish(msg2);
    pub3.publish(msg3);
		
    ROS_INFO_STREAM("vel: " << msg1 << msg2 << msg3);

  }
}
